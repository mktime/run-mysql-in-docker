FROM debian:jessie
ENV DEBIAN_FRONTEND noninteractive

RUN echo "deb http://mirrors.163.com/debian/ jessie main non-free contrib\n deb http://mirrors.163.com/debian/ jessie-updates main non-free contrib\n deb http://mirrors.163.com/debian/ jessie-backports main non-free contrib\n deb-src http://mirrors.163.com/debian/ jessie main non-free contrib \n deb-src http://mirrors.163.com/debian/ jessie-updates main non-free contrib \n deb-src http://mirrors.163.com/debian/ jessie-backports main non-free contrib \n deb http://mirrors.163.com/debian-security/ jessie/updates main non-free contrib \n deb-src http://mirrors.163.com/debian-security/ jessie/updates main non-free contrib" > /etc/apt/sources.list
RUN apt-get update

RUN apt-get -y install mysql-client mysql-server

RUN sed -i -e"s/^bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

ADD ./startup.sh /opt/startup.sh

EXPOSE 3306

CMD ["/bin/bash", "/opt/startup.sh"]
