#!/bin/sh

MYSQL_DATA_PATH=/home/demo/data/mysql

docker run -d -p 3306:3306 --name mysql -v $MYSQL_DATA_PATH:/var/lib/mysql mysql
