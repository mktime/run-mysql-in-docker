#why docker?

### docker is magical
It's my first time to learn docker.

i find that docker is magical.

you can run any app in docker.

it made your pc clean.

i like this feel.

later i'll try to run GUI app(X server) in docker.


### how to start?


`build.sh` is a script to build mysql image.

`create-mysql-container.sh` is a script to create and run mysql container.

`start-server.sh` is a script to start mysql container.

`connect-mysql.sh` is a script to login into the mysql server running in your docker container.

`stop-server.sh` is a script to disconnect mysql server running in your docker container.


before you build your mysql image, you need to modify `startup.sh` and set your mysql user/passwd.

before you run mysql server, you need to modify `create-mysql-container.sh` and set your owe mysql data path.

then, run `connect-mysql.sh' to login you mysql server.

next time you don't need to run `create-mysql-container.sh` again, you just run `start-server.sh` and `connect-mysql.sh`.

###Have fun !
